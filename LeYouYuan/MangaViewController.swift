//
//  MangaViewController.swift
//  LeYou
//
//  Created by Jie Weng on 2021/9/30.
//

import Cocoa

class MangaViewController: NSViewController {
    @IBOutlet weak var tableView: NSTableView!

    private var mangaImagePaths: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
    }

    func updateManga(with model: MangaModel) {
        mangaImagePaths.removeAll()
        mangaImagePaths.append(contentsOf: model.imagePaths)
        tableView.reloadData()
    }
}

extension MangaViewController: NSTableViewDataSource {
    func numberOfRows(in tableView: NSTableView) -> Int {
        mangaImagePaths.count
    }
}

extension MangaViewController: NSTableViewDelegate {
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        guard let cell = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier("MangaCell"), owner: self) as? MangaCellView else {
            return nil
        }
        cell.configure(imagePath: mangaImagePaths[row])
        return cell
    }

    func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat {
        return tableView.frame.width * 3 / 2
    }
}
