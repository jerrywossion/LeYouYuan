//
//  MangaData.swift
//  LeYou (iOS)
//
//  Created by Jie Weng on 2021/9/28.
//

import Foundation

class MangaData: ObservableObject {
    @Published var mangaModels: [MangaModel] = []
    private var path: String

    init(path: String) {
        self.path = path
        self.mangaModels = MangaModel.parseMangaModels(atPath: path)
    }

    convenience init() {
        self.init(path: "Manga")
    }
}
