//
//  MangaCoverItem.swift
//  LeYou
//
//  Created by Jie Weng on 2021/9/29.
//

import Cocoa
import SnapKit

class MangaCoverItem: NSCollectionViewItem {
    static let identifier = NSUserInterfaceItemIdentifier("MangaCoverItem")

    var mangaModel: MangaModel?
    private var coverView: MangaCoverView!

    override func loadView() {
        super.loadView()
        coverView = MangaCoverView()
        view = coverView
    }

    override func prepareForReuse() {
        coverView.configure(title: "", imagePath: nil)
        super.prepareForReuse()
    }

    override func viewWillAppear() {
        super.viewWillAppear()
        coverView.configure(title: mangaModel?.title, imagePath: mangaModel?.coverImagePath)
    }
}
