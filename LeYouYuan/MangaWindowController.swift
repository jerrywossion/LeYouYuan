//
//  MangaWindowController.swift
//  LeYou
//
//  Created by Jie Weng on 2021/9/30.
//

import Cocoa

class MangaWindowController: NSWindowController {
    func updateManga(with model: MangaModel) {
        guard let vc = contentViewController as? MangaViewController else {
            return
        }
        vc.updateManga(with: model)
    }
}
