//
//  ViewController.swift
//  LeYou
//
//  Created by Jie Weng on 2021/9/29.
//

import Cocoa

class MangaCollectionViewController: NSViewController {
    @IBOutlet weak var scrollView: NSScrollView!
    @IBOutlet weak var collectionView: NSCollectionView!

    private let data = MangaData()
    private var mangaWindowController: MangaWindowController?

    override func viewDidLoad() {
        super.viewDidLoad()

        scrollView.automaticallyAdjustsContentInsets = false
        scrollView.contentInsets = NSEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        scrollView.scrollerInsets = NSEdgeInsets(top: 0, left: 0, bottom: 0, right: -15)

        collectionView.register(MangaCoverItem.self, forItemWithIdentifier: MangaCoverItem.identifier)
        let flowLayout = NSCollectionViewFlowLayout()
        flowLayout.itemSize = NSSize(width: 200, height: 300)
        flowLayout.minimumLineSpacing = 20
        flowLayout.minimumInteritemSpacing = 20
        collectionView.collectionViewLayout = flowLayout
        collectionView.dataSource = self
        collectionView.delegate = self
    }
}

extension MangaCollectionViewController: NSCollectionViewDataSource {
    func numberOfSections(in collectionView: NSCollectionView) -> Int {
        1
    }

    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        data.mangaModels.count
    }

    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        let item = collectionView.makeItem(withIdentifier: MangaCoverItem.identifier, for: indexPath)
        guard let item = item as? MangaCoverItem else {
            return item
        }
        item.mangaModel = data.mangaModels[indexPath.item]
        return item
    }
}

extension MangaCollectionViewController: NSCollectionViewDelegate {
    func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {
        guard let windowController = mangaWindowController ?? NSStoryboard.main?.instantiateController(withIdentifier: "MangaWindow") as? MangaWindowController else {
            return
        }
        mangaWindowController = windowController
        windowController.updateManga(with: data.mangaModels[indexPaths.first?.item ?? 0])
        windowController.showWindow(self)
    }
}
