//
//  MangaModel.swift
//  LeYou (iOS)
//
//  Created by Jie Weng on 2021/9/28.
//

import Foundation

extension String {
    static let imageExtensions: [String] = ["jpg", "png"]

    var isImageName: Bool {
        Self.imageExtensions.contains((self as NSString).pathExtension.lowercased())
    }
}

extension FileManager {
    func directoryExists(atPath path: String) -> Bool {
        var isDirectory: ObjCBool = false
        guard self.fileExists(atPath: path, isDirectory: &isDirectory),
              isDirectory.boolValue else {
                  return false
              }
        return isDirectory.boolValue
    }
}

struct MangaModel: Identifiable {
    let title: String
    let path: String
    var coverImagePath: String?
    let imagePaths: [String]

    var id: String {
        path
    }

    init?(path: String) {
        guard FileManager.default.directoryExists(atPath: path) else {
            return nil
        }
        self.path = path
        self.title = (path as NSString).lastPathComponent
        guard let contents = try? FileManager.default.contentsOfDirectory(atPath: path) else {
            return nil
        }

        self.imagePaths = contents
            .filter({ $0.isImageName })
            .sorted()
            .map({ NSString.path(withComponents: [path, $0]) })
        guard !self.imagePaths.isEmpty else {
            return nil
        }
        let metaPath = NSString.path(withComponents: [path, ".meta", "cover.jpg"])
        if FileManager.default.fileExists(atPath: metaPath) {
            self.coverImagePath = metaPath
        } else {
            self.coverImagePath = nil
        }
    }

    static func parseMangaModels(atPath path: String) -> [Self] {
        guard FileManager.default.directoryExists(atPath: path),
              let contents = try? FileManager.default.contentsOfDirectory(atPath: path) else {
            return []
        }
        return contents
            .sorted()
            .map({ NSString.path(withComponents: [path, $0]) })
            .compactMap({ MangaModel(path: $0) })
    }
}
