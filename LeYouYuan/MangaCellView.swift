//
//  MangaCellView.swift
//  LeYou
//
//  Created by Jie Weng on 2021/9/30.
//

import Cocoa

class MangaCellView: NSTableCellView {
    private let mangaImageView = NSImageView()

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        addSubview(mangaImageView)
        mangaImageView.imageScaling = .scaleProportionallyUpOrDown
        mangaImageView.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(20)
        }
    }

    override func prepareForReuse() {
        mangaImageView.image = nil
        super.prepareForReuse()
    }

    func configure(imagePath: String) {
        DispatchQueue.global().async { [weak self] in
            guard let self = self else { return }
            if let image = NSImage(contentsOfFile: imagePath) {
                DispatchQueue.main.async {
                    self.mangaImageView.image = image
                }
            }
        }
    }
}
