//
//  MangaCoverView.swift
//  LeYou
//
//  Created by Jie Weng on 2021/9/30.
//

import Cocoa

class MangaCoverView: NSView {
    private let titleLabel = NSTextField()
    private let coverImageView = NSImageView()

    override var isFlipped: Bool { true }

    private var titleSize: CGSize = .zero
    private let imageTitleSpacing: CGFloat = 5

    init() {
        super.init(frame: .zero)
        addSubview(titleLabel)
        addSubview(coverImageView)

        titleLabel.maximumNumberOfLines = 2
        titleLabel.isEditable = false
        titleLabel.alignment = .center
        titleLabel.isBordered = false
        coverImageView.imageScaling = .scaleProportionallyUpOrDown
        coverImageView.wantsLayer = true
        coverImageView.layer?.backgroundColor = NSColor(red: 238 / 255, green: 238 / 255, blue: 238 / 255, alpha: 1).cgColor
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configure(title: String?, imagePath: String?) {
        titleLabel.stringValue = title ?? ""
        titleSize = titleLabel.sizeThatFits(NSSize(width: frame.width, height: 0))
        titleSize.width = frame.width
        needsLayout = true

        guard let path = imagePath else {
            coverImageView.image = nil
            return
        }

        let remainingSize = NSSize(width: frame.width, height: frame.height - titleSize.height - imageTitleSpacing)
        DispatchQueue.global().async { [weak self] in
            guard let self = self else {
                return
            }

            guard let image = NSImage(contentsOfFile: path) else {
                      return
                  }
            let originSize = image.size
            let scale = remainingSize.height / originSize.height
            let smallImageSize = NSSize(width: originSize.width * scale, height: remainingSize.height)
            let smallImage = NSImage(size: smallImageSize)
            smallImage.lockFocus()
            image.draw(in: NSRect(origin: .zero,
                                  size: smallImageSize),
                       from: NSRect(origin: .zero, size: originSize),
                       operation: .copy,
                       fraction: 1)
            smallImage.unlockFocus()
            DispatchQueue.main.async {
                self.coverImageView.image = smallImage
            }
        }
    }

    override func layout() {
        super.layout()
        titleLabel.frame = NSRect(origin: CGPoint(x: 0, y: frame.height - titleSize.height),
                                  size: titleSize)
        coverImageView.frame = NSRect(origin: CGPoint(x: 0, y: 0),
                                      size: NSSize(width: frame.width,
                                                   height: frame.height - titleSize.height - imageTitleSpacing))
    }
}
